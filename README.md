# flx-ts-test

Test the package [flx-ts](https://github.com/jcs090218/flx-ts).

## Notes (for future me)

### `tsconfig.json` file

```sh
$ tsc --init
```

Make sure these config are enabled (uncomment).

```json
"rootDir": "./src/",
"sourceMap": true,         /* You need this to make the program runnable */
"outDir": "./dist/",       /* Make dist the output dir */
"removeComments": true,
"noEmitOnError": true,     /* Don't generate js files if error occurred */
```

### `.vscode/launch.json` file

Use VSCode's `Run and Debug` panel to create the `.vscode/launch.json` file.

Add the following line to `.vscode/launch.json` file.

```typescript
"preLaunchTask": "tsc: build - tsconfig.json",
```
